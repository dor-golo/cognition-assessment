<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\TicketTypes;

class TicketsController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
    	$tickets = Ticket::sortable(['created_at' => 'desc'])
        ->paginate(10);
        $data = [
            'tickets' => $tickets,
        ];
        return view('tickets', $data);
    }

    public function single($id) {
    	$ticket = Ticket::select('*')
    	->with('user')
        ->with('ticketState')
    	->where('id', '=', $id)
    	->first();

    	return null;
    }

    public function newPage(){
        $ticketTypes = TicketTypes::get();
        $data = [
            'ticketTypes' => $ticketTypes
        ];
        return view('ticketnew', $data); 
    }

    public function saveNew(Request $request){
        $ticket = new Ticket();
        // $array = [
        //     'user_id' => $request->id,
        //     'ticketType_id' => $request->ticketType,
        //     'updated_at' => Carbon\Carbon::now(), 
        //     'created_at' => Carbon\Carbon::now(),
        // ];
        $request->user_id = auth()->user()->id;
        $ticket->saveNew($request->toArray());
        $to_name = $ticketData->first_name.' '.$ticketData->last_name;
        $to_email = $ticketData->email;
        $data = $ticket;
        Mail::send('emails.template', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)->subject('Ticket Mail');
        );
        return back();
    }
}
