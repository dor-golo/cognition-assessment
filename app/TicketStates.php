<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class TicketStates extends Model
{
    protected $table = 'ticket_states';

	public $sortable = [
		'name',
	];

	public function ticket()
    {
        return $this->hasMany('App\Ticket', 'ticketState_id', 'id');
    }
}