<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
Use App\User;

class Ticket extends Model
{
	use Sortable;

	protected $fillable = [
		'first_name',
		'last_name',
		'user_id',
		'email',
		'issue',
		'lat',
		'long',
		'ticketType_id'
	];
    
    protected $attributes = [
       'ticketState_id' => 1,
    ];

	public $sortable = [
		'first_name',
		'last_name',
		'created_at',
		'ticketState_id'
	];

    protected $table = 'tickets';

    public function user()
	{
    	return $this->belongsTo('App\User');
	}

	public function ticketState()
	{
		return $this->belongsTo('App\TicketStates', 'ticketState_id');
	}

	public function saveNew($ticketData){

		$to_name = $ticketData->first_name.' '.$ticketData->last_name;
		$to_email = $ticketData->email;
		$data = $ticketData;
		Mail::send('emails.template', $data, function($message) use ($to_name, $to_email) {
			$message->to($to_email, $to_name)->subject('Ticket Mail');
		);

		return $this->create($ticketData);
	}
}
