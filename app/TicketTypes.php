<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class TicketTypes extends Model
{
    protected $table = 'ticket_types';

	public $sortable = [
		'name',
	];

	public function ticket()
    {
        return $this->hasMany('App\Ticket', 'ticketType_id', 'id');
    }
}