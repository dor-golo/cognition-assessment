@extends('layouts.app')

@section('content')

<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <strong>Inline</strong> Form
        </div>
        <div class="card-body card-block">
            <form action="/tickets/new" method="POST" class="form-inline">
            	@csrf
            	<input style="display: none;" type="text" name="user_id" value="{{Auth::user()->id}}">
                <input id='lat' style="display: none;" type="text" name="lat" value="">
                <input id='long' style="display: none;" type="text" name="long" value="">
                <div class="form-group">
                    <label for="exampleInputName2" class="pr-1  form-control-label">First Name</label>
                    <input type="text" id="exampleInputName2" placeholder="Jane" name='first_name' required="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="exampleInputName2" class="pr-1  form-control-label">Surname</label>
                    <input type="text" id="exampleInputName2" placeholder="Doe" name='last_name' required="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail2" class="px-1  form-control-label">Email</label>
                    <input type="email" id="exampleInputEmail2" name='email' placeholder="jane.doe@example.com" required="" class="form-control">
                </div>
                <div class="row form-group">
                    <div class="col col-md-3">
                        <label for="textarea-input" class=" form-control-label">Issue</label>
                    </div>
                    <div class="col-12 col-md-9">
                        <textarea id="textarea-input" rows="9" name='issue' placeholder="Content..." class="form-control"></textarea>
                    </div>
                </div>
                <div class="row form-group">
	                <div class="col col-md-3">
	                    <label for="select" class=" form-control-label">Department</label>
	                </div>
	                <div class="col-12 col-md-9">
	                    <select name="ticketType_id" id="select" class="form-control">
	                        <option value="0">Please select</option>
	                        @foreach($ticketTypes as $ticketType)
	                        <option value="{{ $ticketType->id }}">{{ $ticketType->name }}</option>
	                        @endforeach
	                    </select>
	                </div>
	            </div>
		        <div class="card-footer">
		            <button type="submit" class="btn btn-primary btn-sm">
		                <i class="fa fa-dot-circle-o"></i> Submit
		            </button>
		            <button type="reset" class="btn btn-danger btn-sm">
		                <i class="fa fa-ban"></i> Reset
		            </button>
		        </div>
            </form>
        </div>
    </div>
</div>

@endsection