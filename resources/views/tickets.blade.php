@extends('layouts.app')

@section('content')

<div class="table-responsive table-responsive-data2">
    <table class="table table-data2">
        <thead>
            <tr>
                <th>@sortablelink('first_name', 'First Name')</th>
                <th>@sortablelink('last_name', 'Last Name')</th>
                <th>@sortablelink('created_at', 'Logged Time')</th>
                <th>@sortablelink('ticketState_id', 'State')</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tickets as $ticket)
        	    <tr class="tr-shadow">
                	<td>{{ $ticket->first_name }}</td>
                	<td>{{ $ticket->last_name }}</td>
                	<td><{{ $ticket->created_at }}</td>
	            	<td><span class="status--process">{{ $ticket->ticketState->name }}</span></td>
	            </tr>
	            <tr class="spacer"></tr>
	            <tr class="tr-shadow">
			@endforeach
        </tbody>
    </table>
    {!! $tickets->appends(\Request::except('page'))->render() !!}
</div>

@endsection