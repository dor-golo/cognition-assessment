<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Section 2</title>

    <!-- Fontfaces CSS-->
    <link href="/css/font-face.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        @guest
        @else
            @include('layouts.partials.nav')
        @endguest
        <div class="main-content">
            @yield('content')
        </div>
    </div>
    <!-- Jquery JS-->
    <script src="{{ URL::asset('vendor/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <!-- Bootstrap JS-->
    <script src="{{ URL::asset('vendor/bootstrap-4.1/popper.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/bootstrap-4.1/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- Vendor JS       -->
    <script src="{{ URL::asset('vendor/slick/slick.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/wow/wow.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/animsition/animsition.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/counter-up/jquery.waypoints.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/counter-up/jquery.counterup.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/circle-progress/circle-progress.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/perfect-scrollbar/perfect-scrollbar.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/chartjs/Chart.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('vendor/select2/select2.min.js') }}" type="text/javascript"></script>

    <!-- Main JS-->
    <script src="{{ URL::asset('js/main.js') }}" type="text/javascript"></script>

</body>

</html>
<!-- end document-->
