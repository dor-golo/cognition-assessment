<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);

Route::get('/', 'HomeController@index')->name('home');

Route::get('/tickets', 'TicketsController@index')
->middleware('auth')
->name('tickets');

Route::get('tickets/new', 'TicketsController@newPage')
->middleware('auth')
->name('new ticket');

Route::post('tickets/new', 'TicketsController@saveNew')
->middleware('auth')
->name('new ticket');

Route::get('/tickets/{id}', 'TikcetsController@single')
->name('ticket');

Route::get('/complex-query', 'complexController@index')
->name('complex');