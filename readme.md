## Required Packages

The following will be a list of packages that are required for the system and the commands to install each of them. Please follow the order in which this done as some packages must be installed before others in order to work. Some of these packages may already exist on your system, if they do you can ignore those sections

<p align="center">
	git
</p>
<p align="center">
	apache server
</p>
<p align="center">
	php
</p>
change the version of php depending on the version you have installed
sudo apt-get install php7.2-mysql
sudo apt-get install php-xml
<p align="center">
	curl
	sudo apt-get install curl
</p>
<p align="center">
	Composer
	sudo curl -s https://getcomposer.org/installer | php
	sudo mv composer.phar /usr/local/bin/composer
</p>

## INSTALLATION

Step 1
run
composer install

Step 2
Create your own .env file using the .env.example as a template

Step 3
Before running any of the migrations create the ticketing schema in you db using the following mysql command:



To genrate the tables in the db and seed them run:
php artisan migrate:fresh --seed

Four users would be generated for the purposes of the ticketing system.

50 tickets will be generated along with the 3 ticket types and 3 ticket states