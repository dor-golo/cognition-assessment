<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PeopleSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 50; $i++){
            DB::table('personalDetails')->insert([
                'first_name' => Str::random(6),
            ]);
        }
    }
}