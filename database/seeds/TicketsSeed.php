<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TicketsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 50; $i++){
            DB::table('tickets')->insert([
                'user_id' => rand(1, 3),
                'lat' => random_float(0, 180),
                'long' => random_float(0, 180),
                'ticketType_id' => rand(1, 3),
                'ticketState_id' => rand(1, 3),
                'email' => Str::random(5).'@email.com',
                'first_name' => Str::random(6),
                'last_name' => Str::random(10),
                'issue' => Str::random(55),
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}

function random_float ($min,$max) {
    return ($min + lcg_value()*(abs($max - $min)));
}
